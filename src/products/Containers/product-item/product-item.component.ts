import { Component, Input, OnInit } from "@angular/core";
import { Pizza } from "../../models/pizza";
import { Topping } from "../../models/topping";
import { PizzasService, ToppingsService } from "../../services";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  pizza: Pizza;
  toppings: Topping[];
  newPizza: Pizza;

  constructor(
    private pizzaService: PizzasService,
    private toppingsService: ToppingsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.pizzaService.getPizzas().subscribe(pizzas => {
      const param = this.route.snapshot.params.id;
      let pizza = undefined;
      if ('new' === param) {
        pizza = {};
      } else {
        pizza = pizzas.find(ele => ele.id === parseInt(param, 10));
      }
      this.pizza = pizza
      this.newPizza = pizza
      this.toppingsService.getToppings().subscribe(tps => this.toppings = tps)

    });
  }
}

import { Component, OnInit } from '@angular/core';
import {PizzasService} from '../../services';
import {Pizza} from '../../models/pizza';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  pizzas: Pizza[]
  constructor(private pizzaService: PizzasService) { }

  ngOnInit() {
    this.pizzaService.getPizzas().subscribe(pizzas => this.pizzas = pizzas, ( error: HttpErrorResponse) => {
        console.log('Error when get pizzas' , error.message)
    })
  }

}

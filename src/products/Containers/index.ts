import {ProductsComponent} from './products/products.component';
import {ProductItemComponent} from './product-item/product-item.component';

export const containers: any[] = [ProductsComponent , ProductItemComponent];

export * from  './product-item/product-item.component';
export * from  './products/products.component';

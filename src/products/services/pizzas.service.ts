import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Pizza} from '../models/pizza';

const urlPizza = 'http://localhost:3000/pizzas';

@Injectable({
  providedIn: 'root'
})
export class PizzasService {

  constructor(private http: HttpClient) {
  }

  getPizzas(): Observable<Pizza[]> {
    return this.http.get<Pizza[]>(urlPizza);

  }

  createPizza(payload: Pizza): Observable<Pizza> {
    return this.http.post<Pizza>(urlPizza, payload);
  }

  updatePiazza(payload: Pizza): Observable<Pizza> {
    return this.http.put<Pizza>(`${urlPizza}/${payload.id}`, payload);

  }

  deletePizza(payload: Pizza): Observable<Pizza>{
      return  this.http.delete<any>(`${urlPizza}/${payload.id}`);
  }
}

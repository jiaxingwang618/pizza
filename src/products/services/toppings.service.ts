import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {observableToBeFn} from 'rxjs/internal/testing/TestScheduler';
import {Topping} from '../models/topping';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToppingsService {

  constructor(private http: HttpClient) { }

  getToppings(): Observable <Topping[]> {
     return  this.http.get<Topping[]> ('/api/toppings');
  }}


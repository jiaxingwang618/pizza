import {Component, Input, OnInit} from '@angular/core';
import {Topping} from '../../models/topping';

@Component({
  selector: 'app-pizza-toppings',
  templateUrl: './pizza-toppings.component.html',
  styleUrls: ['./pizza-toppings.component.scss']
})
export class PizzaToppingsComponent implements OnInit {

  @Input() toppings: Topping[]
  constructor() { }

  ngOnInit() {
  }

}

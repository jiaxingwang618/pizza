import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './Containers/products/products.component';
import { PizzaDisplayComponent } from './componments/pizza-display/pizza-display.component';
import { PizzaFormComponent } from './componments/pizza-form/pizza-form.component';
import { PizzaItemComponent } from './componments/pizza-item/pizza-item.component';
import { PizzaToppingsComponent } from './componments/pizza-toppings/pizza-toppings.component';
import { ProductItemComponent } from './Containers/product-item/product-item.component';
import {RouterModule, Routes} from '@angular/router';
import * as fromContainers from './Containers';
import {ReactiveFormsModule} from '@angular/forms';

export const ROUTES: Routes = [
  {
    path: '',
    component: fromContainers.ProductsComponent
  }
  ,
  {
    path: ':id',
    component: fromContainers.ProductItemComponent
  },
  {
    path: 'new',
    component: fromContainers.ProductItemComponent
  }
];

@NgModule({

  declarations: [ProductsComponent, PizzaDisplayComponent, PizzaFormComponent, PizzaItemComponent, PizzaToppingsComponent, ProductItemComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    ReactiveFormsModule
  ]
})
export class ProductsModule { }
